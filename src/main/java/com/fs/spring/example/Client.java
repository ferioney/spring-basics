package com.fs.spring.example;

import org.springframework.beans.factory.annotation.Value;

public class Client {
    @Value("1")
    private String id;

    @Value("Smith")
    private String fullName;

    @Value("Hi new user!")
    private String greeting;

    public String getGreeting() {
        return greeting;
    }

    public void setGreeting(String greeting) {
        this.greeting = greeting;
    }

    public String getId() {
        return id;
    }

    public String getFullName() {
        return fullName;
    }

}
