package com.fs.spring.example;

public enum EventType {
    INFO, ERROR;
}
