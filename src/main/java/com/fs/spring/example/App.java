package com.fs.spring.example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Map;

@Service
public class App {
    private Client client;

    @Resource(name = "loggers")
    private Map<EventType, EventLogger> loggers;

    @Autowired
    public App(Client client, Map<EventType, EventLogger> loggers) {
        this.client = client;
        this.loggers = loggers;
    }

    public void logEvent(Event event, EventType type) {
        EventLogger logger = this.loggers.get(type);

        String formatMessage = event.getMessage().replaceAll(client.getId(), client.getFullName());
        event.setMessage(formatMessage);
        logger.logEvent(event);
    }

    public static void main(String[] args) throws InterruptedException {
        ApplicationContext cxt = new AnnotationConfigApplicationContext(AppConfig.class);

        App app = (App) cxt.getBean("app");

        Event event = (Event) cxt.getBean("event");
        event.setMessage("Some event for user 1");
        app.logEvent(event, EventType.INFO);

        Thread.sleep(1000);

        event = (Event) cxt.getBean("event");
        event.setMessage("Some event for user 2");
        app.logEvent(event, EventType.ERROR);
    }

}
