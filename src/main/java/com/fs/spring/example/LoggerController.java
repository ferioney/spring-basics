package com.fs.spring.example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoggerController {

    @Autowired
    private Event event;

    @Autowired
    private App application;

    @RequestMapping("/")
    public String greeting() {
        event.setMessage("Some event for user 1");
        application.logEvent(event, EventType.INFO);
        return "Hi User!";
    }
}
