package com.fs.spring.example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.util.EnumMap;
import java.util.Map;

@Configuration
@ComponentScan
public class AppConfig {

    @Autowired
    private EventLogger consoleEventLogger;

    @Autowired
    @Qualifier("fileEventLogger")
    private EventLogger fileLogger;

    @Bean
    public Client client() {
        return new Client();
    }

    @Bean
    public Map<EventType, EventLogger> loggers() {
        Map map = new EnumMap<>(EventType.class);
        map.put(EventType.INFO, consoleEventLogger);
        map.put(EventType.ERROR, fileLogger);
        return map;
    }

}
