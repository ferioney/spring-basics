package com.fs.spring.example;

public interface EventLogger {
    public void logEvent(Event event);
}
