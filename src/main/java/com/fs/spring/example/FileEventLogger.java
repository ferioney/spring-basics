package com.fs.spring.example;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;

@Component
public class FileEventLogger implements EventLogger {

    @Value("logFile")
    private String fileName;
    private File file;

    @PostConstruct
    public void init() throws IOException {
        this.file = new File(fileName);
        if (this.file.exists() && !file.canWrite()) {
            throw new IllegalArgumentException("Can not write to the file " + fileName);
        } else if (!this.file.exists()) {
            file.createNewFile();
        }
    }

    @Override
    public void logEvent(Event event) {
        try {
            FileUtils.writeStringToFile(file, event.toString() + "\n", true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
